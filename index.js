"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.thousandSeparator = void 0;
const thousandSeparator_1 = __importDefault(require("./thousandSeparator"));
exports.thousandSeparator = thousandSeparator_1.default;
console.log((0, exports.thousandSeparator)(1000));

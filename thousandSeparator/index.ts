const thousandseparator = (x: Number) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};

export default thousandseparator;

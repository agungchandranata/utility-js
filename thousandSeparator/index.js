"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const thousandseparator = (x) => {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
};
exports.default = thousandseparator;
